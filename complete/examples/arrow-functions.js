let person = {
    name: 'John',
    sayHi: function () {
        return this.name;
    },
    determineContext: function () {
        return this;
    }
}

let dog = {
    sayHi: () => {
        return this.name;
    },
    determineContext: () => {
        return this;
    }
}
//in case of arrow functions this always refer to global context
console.log(person.sayHi());
console.log(person.determineContext());
console.log(person.determineContext() === this);
console.log(dog.sayHi());
console.log(dog.determineContext() === this);

//this in calling braces refer to global context
