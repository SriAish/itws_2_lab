// The let statement declares a block scope local variable

// var example
function count1() {
    for (var i = 1; i <= 5; i++) {
        setTimeout(() => {
            console.log(i);
        }, 1000);
    }
//prints 6 every time
//as setTimeout is asynch and it waits for 1sec but for loop keeps on going
//therefore by the time it starts printing valus is 6 
}
count1();

// let example

function count2() {
    for (let i = 1; i <= 5; i++) {
        setTimeout(() => {
            console.log(i);
        }, 1000);
    }
//prints 1 2 3 4 5
//as let is block specific and is not available outside
//therefore 5 blocks created which has its own i so it prints val of its specific i
}
count2();
