/* The Promise object represents the eventual completion (or failure) of an asynchronous operation, and its
 resulting value. */

function count3() {
    setTimeout(function () {
        console.log(1);
        setTimeout(function () {
            console.log(2);
            setTimeout(function () {
                console.log(3);
                console.log('count done');
            }, 1000);
        }, 1000);
    }, 1000);
}
// prints 1 2 3
//as setTimeout counts one by one
count3();

function makePromise(i) {
    return new Promise((resolve) => {
        setTimeout(function () {
            console.log(i);
            resolve();
        }, 1000)
    })
}
//promise asynchronous i.e. keeps on going after caling it doesnt wait for called func to return 
function newCount31() {
    makePromise(1)
        .then(() => {
            return makePromise(2);
        })
        .then(() => {
            return makePromise(3);
        })
        .then(() => {
            console.log('newCount311 done');
        })
//then acts as the resolve func for promise
//we keep on chaining promises until we want
//and then end with some other func
}
newCount31();

function makePromise(i) {
    return new Promise((resolve) => {
        setTimeout(function () {
            console.log(i);
            resolve(i);
        }, 1000)
    })
}
//promise asynchronous i.e. keeps on going after caling it doesnt wait for called func to return 
function newCount3() {
    makePromise(1)
        .then((x) => {
            console.log(x);
            return makePromise(2);
        })
        .then((x) => {
            console.log(x);
            return makePromise(3);
        })
        .then((x) => {
            console.log(x);
            console.log('newCount3 done');
        })
//then acts as the resolve func for promise
//we keep on chaining promises until we want
//and then end with some other func
}
newCount3();
