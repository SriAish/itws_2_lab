/* keyword 'this' is defined when a function is run. If there's no function running then value of 'this' does not
 change */
//back tick is used for string subsitution
let person = {
    name: 'John1',
    sayHi: function () {
        setTimeout(function () {
            console.log(`${this.name} says Hi !!`);
        }, 1000);
    }
}
//1000=1sec in ^^^ this becomes = window
person.sayHi();

/* using bind to change the context of 'this' */
let newPerson = {
    name: 'John2',
    sayHi: function () {
        setTimeout(function () {
            console.log(`${this.name} says Hi !!`);
        }.bind(this), 1000);
    }
}
//bind makes func use the context we given
//bind just defines context
//apply also calls func immediately
newPerson.sayHi();
