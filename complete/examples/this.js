/* 'this' is a keyword whose value depends on the execution context at runtime */

/* Global context */
//console.log(this);

function whatIsThis() {
    return this;
}

//console.log(whatIsThis());

function variablesInThis() {
    this.person = 'John';
}

variablesInThis();
//console.log(person);

/* Object context */
let newPerson = {
    name: 'John',
    sayHi: function () {
        return this.name;
    },
    determineContext: function () {
        return this === newPerson;
    },
    dog: {
        sayHi: function () {
            return this.name;
        },
        determineContext: function () {
            return this === newPerson;
        }
    }
}

console.log(newPerson.sayHi());
//no error
console.log(newPerson.determineContext());
console.log(newPerson.dog.sayHi());
//undefined as dog doesnt have name attribute
console.log(newPerson.dog.determineContext());
console.log(newPerson.dog.sayHi.apply(newPerson));
//apply tells what we have to take as this
console.log(newPerson.dog.determineContext.apply(newPerson));
