function Person(a,b){
	this.first = a;
	this.last = b;
}

var person = new Person('John','Legend');

//details of objecr
console.log(person);
//function used to make object
console.log(person.constructor);
//prototypes not for objects only for functions
console.log(person.prototype);
//if a prototype used
console.log(Person.prototype);
console.log(Person.prototype);
console.log(person.__proto__ === Person.prototype);
