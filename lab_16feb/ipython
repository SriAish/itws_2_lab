In [1]: x=1

In [2]: x
Out[2]: 1

In [3]: x?
Type:        int
String form: 1
Docstring:
int(x=0) -> int or long
int(x, base=10) -> int or long

Convert a number or string to an integer, or return 0 if no arguments
are given.  If x is floating point, the conversion truncates towards zero.
If x is outside the integer range, the function returns a long instead.

If x is not a number or if base is given, then x must be a string or
Unicode object representing an integer literal in the given base.  The
literal can be preceded by '+' or '-' and be surrounded by whitespace.
The base defaults to 10.  Valid bases are 0 and 2-36.  Base 0 means to
interpret the base from the string as an integer literal.
>>> int('0b100', base=0)
4

In [4]: x.bit_length()
Out[4]: 1

In [5]: x.bit_length()?
  File "<ipython-input-5-d6f32e438853>", line 1
    x.bit_length()?
                  ^
SyntaxError: invalid syntax


In [6]: x.bit_length?
Docstring:
int.bit_length() -> int

Number of bits necessary to represent self in binary.
>>> bin(37)
'0b100101'
>>> (37).bit_length()
6
Type:      builtin_function_or_method

In [14]: x="hello world"

In [15]: x
Out[15]: 'hello world'

In [16]: x.capitalize()
Out[16]: 'Hello world'

In [17]: x*2
Out[17]: 'hello worldhello world'

In [18]: x*3
Out[18]: 'hello worldhello worldhello world'

In [19]: y=2

In [20]: 2*2
Out[20]: 4

In [21]: y*2
Out[21]: 4

In [25]: x=True

In [26]: x
Out[26]: True

In [27]: x?
Type:        bool
String form: True
Docstring:
bool(x) -> bool

Returns True when the argument x is true, False otherwise.
The builtins True and False are the only two instances of the class bool.
The class bool is a subclass of the class int, and cannot be subclassed.

In [28]: x = [1,2,3,4,5]

In [29]: x
Out[29]: [1, 2, 3, 4, 5]

In [30]: x?
Type:        list
String form: [1, 2, 3, 4, 5]
Length:      5
Docstring:
list() -> new empty list
list(iterable) -> new list initialized from iterable's items

In [32]: x
Out[32]: (1, 2, 3, 4, 5)

In [33]: x?
Type:        tuple
String form: (1, 2, 3, 4, 5)
Length:      5
Docstring:
tuple() -> empty tuple
tuple(iterable) -> tuple initialized from iterable's items

If the argument is a tuple, the return value is the same object.

In [34]: x[1] = 2
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-34-21ec69ae11d2> in <module>()
----> 1 x[1] = 2

TypeError: 'tuple' object does not support item assignment

//tuples are immutable therefore we cant assign values to its elements

In [35]: x = {}

In [36]: x?
Type:        dict
String form: {}
Length:      0
Docstring:
dict() -> new empty dictionary
dict(mapping) -> new dictionary initialized from a mapping object's
    (key, value) pairs
dict(iterable) -> new dictionary initialized as if via:
    d = {}
    for k, v in iterable:
        d[k] = v
dict(**kwargs) -> new dictionary initialized with the name=value pairs
    in the keyword argument list.  For example:  dict(one=1, two=2)

In [37]: x[1] = 2

In [38]: x['1'] = 3

In [39]: x
Out[39]: {1: 2, '1': 3}

In [40]: x[1]+x['1']
Out[40]: 5

In [41]: x
Out[41]: {1: 2, '1': 3}

In [42]: if x[1] != x['1']:
   ....:     printf('ok')
   ....:     print('ok')
   ....:
KeyboardInterrupt

In [42]: if x[1] != x['1']:
   ....:     print('ok')
   ....:
KeyboardInterrupt

In [42]: if x[1] != x['1']:
   ....:     print('OK')
   ....:
OK

In [43]: for t in x
  File "<ipython-input-43-e2b95f009c9d>", line 1
    for t in x
              ^
SyntaxError: invalid syntax


In [44]: for t in x:
   ....:     print(t)
   ....:
1
1

In [45]: for t in x:
    print(x[t])
   ....:
3
2

In [46]: x?
Type:        dict
String form: {'1': 3, 1: 2}
Length:      2
Docstring:
dict() -> new empty dictionary
dict(mapping) -> new dictionary initialized from a mapping object's
    (key, value) pairs
dict(iterable) -> new dictionary initialized as if via:
    d = {}
    for k, v in iterable:
        d[k] = v
dict(**kwargs) -> new dictionary initialized with the name=value pairs
    in the keyword argument list.  For example:  dict(one=1, two=2)

In [47]: x.items
Out[47]: <function items>

In [48]: x.items()
Out[48]: [('1', 3), (1, 2)]

///////////////////
In [1]: for x in range(10)
  File "<ipython-input-1-9a70f2057d47>", line 1
    for x in range(10)
                      ^
SyntaxError: invalid syntax


In [2]: for x in range(10):
   ...:     print(x)
   ...:     
0
1
2
3
4
5
6
7
8
9
//////////////////

In [49]: # hello

In [50]: range(10)
Out[50]: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

In [51]: xrange(10)
Out[51]: xrange(10)

In [52]: x = range(10)

In [53]: x
Out[53]: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

In [54]: xrange(1,10)
Out[54]: xrange(1, 10)

In [55]: xrange(1,10,1)
Out[55]: xrange(1, 10)

In [56]: xrange(1,10,2)
Out[56]: xrange(1, 11, 2)

In [57]: x.start
---------------------------------------------------------------------------
AttributeError                            Traceback (most recent call last)
<ipython-input-57-e0dbb33aba45> in <module>()
----> 1 x.start

AttributeError: 'list' object has no attribute 'start'

In [65]: def x():
   ....:     print('Hello')
   ....:

In [66]: x?
Signature: x()
Docstring: <no docstring>
File:      ~/<ipython-input-65-ad14e85c87f0>
Type:      function

In [67]: x()
Hello

In [68]: len('1234')
Out[68]: 4

In [69]: len?
Docstring:
len(object) -> integer

Return the number of items of a sequence or collection.
Type:      builtin_function_or_method

In [70]: x=[0]*3

In [71]: x
Out[71]: [0, 0, 0]

In [72]: x=[1,2,3]*3

In [73]: x
Out[73]: [1, 2, 3, 1, 2, 3, 1, 2, 3]

In [74]: x[1]=4
//primitive changes value of only 1 element
In [75]: x
Out[75]: [1, 4, 3, 1, 2, 3, 1, 2, 3]

In [76]: x=[[0]]*3

In [77]: x
Out[77]: [[0], [0], [0]]

In [78]: x[1].append(2)
//here its refering to same value
In [79]: x
Out[79]: [[0, 2], [0, 2], [0, 2]]

In [85]: x=[[] for x in range(5)]

In [86]: x
Out[86]: [[], [], [], [], []]

In [87]: x[0].append(1)

In [88]: x
Out[88]: [[1], [], [], [], []]

In [118]: x.append(1)

In [119]: x
Out[119]: [[], [], [], [], [], 1]

In [120]: x[0].append(1)

In [121]: x.pop
Out[121]: <function pop>

In [122]: x.pop()
Out[122]: 1

In [123]: x
Out[123]: [[1], [], [], [], []]
In [124]: y = (x for x in range(5))

In [125]: y
Out[125]: <generator object <genexpr> at 0x7fa19402ee60>

In [126]: list(y)
Out[126]: [0, 1, 2, 3, 4]

In [128]: map(lambda x: x**2,[1,2,3,4])
Out[128]: [1, 4, 9, 16]

In [129]: x = map(lambda x: x**2,[1,2,3,4])

In [130]: list(x)
Out[130]: [1, 4, 9, 16]
//works in python3 

In [132]: next(x)
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-132-5e4e57af3a97> in <module>()
----> 1 next(x)

TypeError: list object is not an iterator
In [138]: from os import system

In [139]: system(ls)
---------------------------------------------------------------------------
NameError                                 Traceback (most recent call last)
<ipython-input-139-7a90aaba211e> in <module>()
----> 1 system(ls)

NameError: name 'ls' is not defined

In [140]: system('ls')
20171191		       calc.html  CSO.tar.gz  folder		 'game (1)'  game.zip  hello	   labb      Pictures	  riscv-tools.tar.gz
20171191.tar.xz		       calc.js	  Desktop     gallery-start	 game1.js    h11.html  index.html  lab.html  pro.html	  Sanjana
Bluespec		       count.js   Documents   gallery-start.zip  game.css    h1.html   it.js	   lastlab   promise.js   saujas
Bluespec-2016.07.beta1.tar.gz  CSO	  Downloads   game		 game.html   h1.js     itws_2_lab  nCr.c~    riscv-tools  Yashas
Out[140]: 0

In [141]: from sys import argv

In [142]: argv?
Type:        list
String form: ['/usr/bin/ipython']
Length:      1
Docstring:
list() -> new empty list
list(iterable) -> new list initialized from iterable's items

In [143]:  In [141]: from sys import argv

In [144]: 

In [144]: In [142]: argv?
Type:        list
String form: ['/usr/bin/ipython']
Length:      1
Docstring:
list() -> new empty list
list(iterable) -> new list initialized from iterable's items

In [145]: Type:        list
  File "<ipython-input-145-1bf393f08959>", line 1
    Type:        list
        ^
SyntaxError: invalid syntax


In [146]: String form: ['/usr/bin/ipython']
  File "<ipython-input-146-c7d836bcc1f1>", line 1
    String form: ['/usr/bin/ipython']
              ^
SyntaxError: invalid syntax


In [147]: Length:      1
  File "<ipython-input-147-66360cb81058>", line 1
    Length:      1
          ^
SyntaxError: invalid syntax


In [148]: Docstring:
   .....:     list() -> new empty list
   .....:     list(iterable) -> new list initialized from iterable's items
   .....:     
  File "<ipython-input-148-bf985b1745e2>", line 1
    Docstring:
             ^
SyntaxError: invalid syntax


In [149]: __name__
Out[149]: '__main__'

In [187]: import os

In [188]: os.__name__
Out[188]: 'os'

In [195]: def y():
   .....:     print("Babe")
   .....:     

In [196]: y()
Babe

In [208]: def x(f):
   .....:     def y(a,b):
   .....:         print('before f')
   .....:         print(f(a,b))
   .....:         print('after f')
   .....:     return y
   .....: 

In [209]: @x
   .....: def z(a,b):
   .....:     return a+b
   .....: 

In [210]: z(1,2)
before f
3
after f


In [214]: def a(v,w):
    return v+w
   .....:

In [215]: a = x(a);

In [216]: a()
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-216-72f2e37b262f> in <module>()
----> 1 a()

TypeError: y() takes exactly 2 arguments (0 given)

In [217]: a(1,2)
before f
3
after f

